//
//  SongViewController.h
//  HV3
//
//  Created by Matjaz Debelak on 7/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Songbook.h"

@interface SongViewController : UIViewController {
	Songbook *songbook;
	NSString *song;
    IBOutlet UIWebView* text;
    IBOutlet UILabel* copyright;
}

-(void) setSongbook: (Songbook*)s;
-(void) setSong: (NSString*)s;

@end
