//
//  SongViewController.m
//  HV3
//
//  Created by Matjaz Debelak on 7/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SongViewController.h"


@implementation SongViewController

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *html = [NSString stringWithFormat: @"<html>\
    <head>\
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\
    <meta name = \"viewport\" content = \"width = device-width\">\
    <meta name = \"viewport\" content = \"initial-scale = 1.0, user-scalable = no\">\
    <style>\
    %@ \
    </style>\
    </head>\
                      <body>", [songbook getStyle]];
    [text loadHTMLString: [html stringByAppendingString:[songbook getTextForNumber:song]] baseURL:nil];
    [copyright setText: [songbook getCopyright]];
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

-(void) setSongbook: (Songbook*)s
{
    songbook = s;
}

-(void) setSong: (NSString*)s
{
    song = s;
}


@end
