//
//  Songbook.m
//  SongbookReader
//
//  Created by Matjaz Debelak on 7/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Songbook.h"


@implementation Songbook

- (id)initWith: (NSString*)songbook
{
    self = [super init];
    if (self) {
        filename = songbook; 
        [filename retain];
        isOpen = NO;
    }
    return self;
}

- (BOOL)open
{
    if (sqlite3_open([filename UTF8String], &db) == SQLITE_OK)
    {
        isOpen = YES;
        return YES;
    }
    else 
    {
        isOpen = NO;
        return NO;
    }
}

- (NSString*)getMetaValueForKey: (NSString*) key
{
    sqlite3_stmt *statement;
	NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM meta WHERE key = '%@'", key];
	const char *query_stmt = [querySQL UTF8String];
	sqlite3_prepare_v2(db, query_stmt, -1, &statement, NULL);
	
    NSString *value = @"";
    
	if (sqlite3_step(statement) == SQLITE_ROW)
	{
		value = [[NSString alloc] initWithUTF8String:
                 (const char *) sqlite3_column_text(statement, 1)];
    }
	sqlite3_finalize(statement);
    return value;
}

- (NSString*)getName
{
    return [self getMetaValueForKey:@"title"];
}

- (NSString*)getTextForNumber: (NSString*)nr
{
    [self open];
    
	sqlite3_stmt *statement;
	NSString *querySQL = [@"SELECT * FROM songs WHERE number = " stringByAppendingString:nr];
	const char *query_stmt = [querySQL UTF8String];
    sqlite3_prepare_v2(db, query_stmt, -1, &statement, NULL);

    NSString* text = @"";
    
	while (sqlite3_step(statement) == SQLITE_ROW)
	{
		NSString *number = [[NSString alloc] initWithUTF8String:
							(const char *) sqlite3_column_text(statement, 0)];
		NSString *author = [[NSString alloc] initWithUTF8String:
							(const char *) sqlite3_column_text(statement, 1)];
		NSString *melody = [[NSString alloc] initWithUTF8String:
							(const char *) sqlite3_column_text(statement, 2)];
		NSString *dur = [[NSString alloc] initWithUTF8String:
						 (const char *) sqlite3_column_text(statement, 3)];
		NSString *country = [[NSString alloc] initWithUTF8String:
							 (const char *) sqlite3_column_text(statement, 4)];
		
		text = [NSString stringWithFormat:@"<div id =\"meta\"><p class=\"number\">%@</p> <p class=\"sep\">•</p> <p class=\"author\">%@</p> <p class=\"sep\">•</p> <p class=\"mel\">Melodie: %@ <p>%@</p></p> <p class=\"sep\">•</p> <p class=\"country\">%@</p></div>\n\n", number, author, melody, dur, country];
		
		[author release];
		[number release];
		[melody release];
		[country release];
		[dur release];
	}
	sqlite3_finalize(statement);
	sqlite3_stmt *statement2;
    
	querySQL = [@"SELECT * FROM verses WHERE song_no = " stringByAppendingString:nr];
	query_stmt = [querySQL UTF8String];
	sqlite3_prepare_v2(db, query_stmt, -1, &statement2, NULL);
	while (sqlite3_step(statement2) == SQLITE_ROW)
	{
		NSString *verse_no = [[NSString alloc] initWithUTF8String:
                              (const char *) sqlite3_column_text(statement, 2)];
		NSString *type = [[NSString alloc] initWithUTF8String:
                          (const char *) sqlite3_column_text(statement, 3)];
		NSString *txt = [[NSString alloc] initWithUTF8String:
                         (const char *) sqlite3_column_text(statement, 4)];
		NSString *ref = [[NSString alloc] initWithUTF8String:
						 (const char *) sqlite3_column_text(statement, 5)];
		
		if ([type isEqualToString:@"ref"])
		{
			verse_no = @"Ref ";
		}
		
		text = [text stringByAppendingFormat:@"<pre>%@:\n %@</pre>\n", verse_no, txt];
		
		[verse_no release];
		[type release];
		[txt release];
		[ref release];
	}
	sqlite3_finalize(statement2);
    return text;
}

- (NSString*)getCopyright
{
        return [@"© " stringByAppendingString:[self getMetaValueForKey:@"copyright"]];
}

- (NSString*)getStyle;
{
    return [self getMetaValueForKey:@"style"];
}

@end
