//
//  Songbook.h
//  SongbookReader
//
//  Created by Matjaz Debelak on 7/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>


@interface Songbook : NSObject {
    NSString *filename;
    sqlite3 *db;
    BOOL isOpen;
}

- (id)initWith: (NSString*)songbook;
- (BOOL)open;
- (NSString*)getName;
- (NSString*)getCopyright;
- (NSString*)getStyle;
- (NSString*)getTextForNumber: (NSString*)nr;

@end
