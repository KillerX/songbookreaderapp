//
//  RootViewController.h
//  HV3
//
//  Created by Matjaz Debelak on 7/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UITableViewController {
	NSMutableArray *songbooks;
}

-(void) refresh;

@end
