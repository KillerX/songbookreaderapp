//
//  SongbookViewController.h
//  HV3
//
//  Created by Matjaz Debelak on 7/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Songbook.h"


@interface SongbookViewController : UIViewController {
	Songbook *songbook;
	IBOutlet UITextField *songNumberText;
}

-(void)setSongbook: (Songbook*)sb;
-(IBAction)displaySong: (id)sender;

@end
